import * as d3 from 'd3';

async function drawLineChart() {
    try {
        const data = await d3.json("../data/newline/nyc_weather.json");
        console.log(data);
    } catch (e) {
        console.log("error : " + e);
    }
}

const WeatherNYC = () => {
    drawLineChart();

    return (
        <div>
            Hello
        </div>
    )
};

export default WeatherNYC;