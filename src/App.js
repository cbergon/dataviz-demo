import { BrowserRouter as Router, Route, Switch, } from "react-router-dom";

import AppLayout from './layout/AppLayout';
import Dataviz from './layout/Dataviz';

import UdemyScatterPlot from './udemy/UdemyScatterPlot';
import WeatherNYC from './newline/WeatherNYC';
import 'antd/dist/antd.css';

function App() {
  return (
    <Router>
      <AppLayout>
        <Switch>
          <Route path="/viz/line-chart/weather/intro" component={WeatherNYC} />
          <Route path="/viz/scatterplot-chart/udemy/intro" component={UdemyScatterPlot} />
          <Route path="/viz" component={Dataviz} />
        </Switch>
      </AppLayout>
    </Router>
  );
};

export default App;
