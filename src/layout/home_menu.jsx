import { BuildTwoTone, IdcardTwoTone, } from '@ant-design/icons';

const home = {
    title: "Who am I ?",
    base_url: "/home",
    prefix: "home",
    subElements: [
        {
            title: "Professionnal experiences",
            icon: (<IdcardTwoTone />),
            base_url: "/home/pro",
            objects: [
                {
                    title: "Media group",
                    url: "/home/pro/media-group",
                },
                {
                    title: "Entrepreneurship",
                    url: "/home/pro/entrepreneurship",
                },
            ] 
        },
        {
            title: "Education",
            icon: (<BuildTwoTone />),
            base_url: "/home/educ",
            objects: [
                {
                    title: "Entrepreneurship & Innovation certificate",
                    url: "/home/educ/matrice",
                },
                {
                    title: "42",
                    url: "/home/educ/42",
                },
                {
                    title: "Accounting Bsc",
                    url: "/home/educ/accounting",
                },
            ]
        },
    ]
};

export default home;