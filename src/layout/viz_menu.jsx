import { LineChartOutlined, DotChartOutlined, } from '@ant-design/icons';

const viz = {
    title: "Data vizualisation",
    prefix: "viz",
    base_url: "/viz",
    subElements: [
        {
            title: "Line Chart",
            base_url: "/viz/line-chart",
            icon: (<LineChartOutlined />),
            objects: [
                {
                    title: "Introduction",
                    url: "/viz/line-chart/weather/intro",
                },
                {
                    url: "/viz/line-chart/weather/viz",
                    title: "Visualisation"
                },
                {
                    url: "/viz/line-chart/weather/table",
                    title: "Table"
                },
            ]
        },
        {
            title: "ScatterPlot Chart",
            base_url: "/viz/scatterplot-chart",
            icon: (<DotChartOutlined />),
            objects: [
                {
                    title: "Introduction",
                    url: "/viz/scatterplot-chart/udemy/intro",
                },
                {
                    title: "Visualisation",
                    url: "/viz/scatterplot-chart/udemy/viz",
                },
                {
                    title: "Table",
                    url: "/viz/scatterplot-chart/udemy/table",
                },
            ] 
        },
    ]
};

export default viz;