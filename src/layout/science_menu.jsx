import { TableOutlined, } from '@ant-design/icons';

const science = {
    title: "Data science",
    prefix: "science",
    base_url: "/science",
    subElements: [
        {
            title: "Science 1",
            icon: (<TableOutlined />),
            base_url: "/science/sc1",
            objects: [
                {
                    title: "Introduction",
                    url: "/science/sc1/intro",
                },
                {
                    title: "Table",
                    url: "/science/sc1/table",
                },
            ] 
        },
        {
            title: "Science 2",
            base_url: "/science/sc2",
            icon: (<TableOutlined />),
            objects: [
                {
                    title: "Introduction",
                    url: "/science/sc2/intro",
                },
                {
                    title: "Table",
                    url: "/science/sc2/table",
                },
            ]
        },
    ]
};

export default science;