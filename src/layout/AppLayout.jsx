import { Layout, Menu, Breadcrumb } from 'antd';
import { Link, useLocation } from 'react-router-dom';
import {
    HomeOutlined, DotChartOutlined,
    ClearOutlined, TableOutlined,
    PlayCircleOutlined, 
} from '@ant-design/icons';

import home from './home_menu';
import viz from './viz_menu';
import science from './science_menu';
import cleansing from './cleansing_menu';

import '../css/layout/AppLayout.css'

const { SubMenu } = Menu;
const { Header, Content, Footer, Sider } = Layout;

function AppLayout(props) {
    const { children } = props;
    const path = useLocation();
    const pathSnippets = path.pathname.split('/').filter(i => i);
    const root = "/" + pathSnippets[0];

    const current = root + "/" + pathSnippets[1];

    const elements = root === "/viz" 
                    ? viz 
                    : root === "/cleansing" 
                    ? cleansing 
                    : root === "/science"
                    ? science : home;


    const breadcrumbs = [];
    if (pathSnippets) {
        if (pathSnippets.length > 0) {
            breadcrumbs.push(<Breadcrumb.Item key="1"><Link to={root}>{elements.title}</Link></Breadcrumb.Item>);
        }
        if (pathSnippets.length > 1) {
            const subEl = elements.subElements.find(o => o.base_url === current);
            breadcrumbs.push(<Breadcrumb.Item key="2"><Link to={current}>{subEl.title}</Link></Breadcrumb.Item>);
            const last = pathSnippets[pathSnippets.length-1];
            if (pathSnippets[1] !== last) {
                const obj = subEl.objects.find(({url}) => url.endsWith(last));
                console.log(obj, last);
                breadcrumbs.push(<Breadcrumb.Item key="3"><Link to={obj.url}>{obj.title}</Link></Breadcrumb.Item>);
            }
        }
    }

    return (
        <Layout>
            <Header className="header">
                <div className="logo" />
                <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['3']}>
                    <Menu.Item key="0">
                        <Link to="/home">
                            <HomeOutlined />
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="1" icon={<ClearOutlined />}>
                        <Link to="/cleansing">
                            Data Cleansing
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="2" icon={<TableOutlined />}>
                        <Link to="/science">
                            Data Science
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="3" icon={<DotChartOutlined />}>
                        <Link to="/viz">
                            Data Visualisation
                        </Link>
                    </Menu.Item>
                </Menu>
            </Header>
            <Content className="layout-content">
                <Breadcrumb style={{ margin: '16px 0' }}>
                    <Breadcrumb.Item key="0"><Link to="/home"><HomeOutlined /></Link></Breadcrumb.Item>
                    {breadcrumbs}
                </Breadcrumb>
                <Layout className="site-layout-background">
                    <Sider className="site-layout-background" width={200}>
                        <Menu
                            mode="inline"
                            defaultSelectedKeys={['2']}
                            defaultOpenKeys={['sub1']}
                            style={{ height: '100%' }}
                            theme="dark"
                        >
                            <Menu.Item key="0" icon={<PlayCircleOutlined />}>
                                <Link to={elements.base_url}>Home</Link>
                            </Menu.Item>
                            {
                                elements.subElements.map((e, i) => (
                                    <SubMenu key={elements.prefix + i + 1} icon={e.icon} title={e.title} disabled={e.disabled}>
                                        {
                                            e.objects.map(({ title, url, disabled, }, j) => (
                                                <Menu.Item key={'' + i + 1 + j} disabled={disabled}>
                                                    {url ? (<Link to={url}>{title}</Link>) : title}
                                                </Menu.Item>
                                            ))
                                        }
                                    </SubMenu>
                            ))}
                        </Menu>
                    </Sider>
                    <Content className="layout-container">{children}</Content>
                </Layout>
            </Content>
            <Footer style={{ textAlign: 'center' }}>Data driven showcases © Clémence Bergon</Footer>
        </Layout>
    );
};

export default AppLayout;