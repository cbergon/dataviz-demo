import { ClearOutlined, } from '@ant-design/icons';

const cleansing = {
    title: "Data cleansing",
    prefix: "cleansing",
    base_url: "/cleansing",
    subElements: [
        {
            title: "Cleansing 1",
            icon: (<ClearOutlined />),
            objects: [
                {
                    title: "Introduction",
                    url: "/cleansing",
                },
                {
                    title: "Table",
                    url: "/cleansing",
                },
            ] 
        },
        {
            title: "Cleansing 2",
            icon: (<ClearOutlined />),
            objects: [
                {
                    title: "Introduction",
                    url: "/cleansing",
                },
                {
                    title: "Table",
                    url: "/cleansing",
                },
            ]
        },
    ]
};

export default cleansing;