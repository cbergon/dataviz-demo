import { useState } from "react";
import { Row, Col, Typography, } from "antd";

import ChartWrapper from "./ChartWrapper";
import TableGen from "./TableGen";

import rawData from '../data/dataviz/udemy-scatterplot.json';
import ScatterPlot from "./ScatterPlot";

const { Title, Text, Link } = Typography;

const UdemyScatterPlot = () => {
    const [data, setData] = useState(rawData);
    const [activeName, setActiveName] = useState(null);
    return (
        <div>
            <Row>
                <Col flex="auto" style={{ marginLeft: 8, marginTop: 8 }}>
                    <Title level={3}>
                        Height according to age and gender for children
                    </Title>
                </Col>
            </Row>
            <Row>
                <Col flex="auto" style={{ marginLeft: 8, marginBottom: 24 }}>
                    <Text>
                        Here is a ScatterPlot Chart connected to a table. Updating data from the table also updates the chart and adapts its context (axis). This example is based on Udemy course 
                        <Link 
                            target="_blank"
                            style={{marginLeft: 4, marginRight: 4}}
                            href="https://www.udemy.com/course/d3-react"
                        >
                            Introducting D3.js with React
                        </Link>
                        by Adam Janes.
                    </Text>
                </Col>
            </Row>
            <Row 
                gutter={[16, 16]}
                align="middle"
            >
                <Col span={12}>
                    <ChartWrapper
                        constructor={ScatterPlot}
                        data={data}
                        activeName={activeName}
                        setActiveName={setActiveName}
                    />
                </Col>
                <Col span={12}>
                    <TableGen 
                        data={data}
                        setData={setData}
                        activeName={activeName}
                        setActiveName={setActiveName}
                    />
                </Col>
            </Row>
        </div>
    )
}

export default UdemyScatterPlot;