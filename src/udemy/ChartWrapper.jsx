import { Component, } from 'react';

// ChartWrapper is a D3 wrapper taking as props:
//      * a constructor for the chart
//      * an object of optional initialization data
class ChartWrapper extends Component {
    renderChart() {
        if (this.props.data.length === 0) {
            return "No data yet";
        }
    }

    componentDidMount() {
        const {constructor, ...restProps} = this.props;
        this.setState({
            chart: new constructor(this.refs.chart, restProps),
        });
        // const {data, setActiveName} = this.props;
        // this.setState({
        //     chart: new ScatterPlot(this.refs.chart, data, setActiveName)
        // });
    }

    shouldComponentUpdate() {
        return false;
    }

    componentWillReceiveProps(nextProps) {
        console.log(nextProps);
        this.state.chart.update(nextProps);
        // const {data, activeName} = nextProps;
        // this.state.chart.update(data, activeName);
    }

    render() {
        return (
            <div className="chart-area" ref="chart"></div>
        )
    }
}

export default ChartWrapper;