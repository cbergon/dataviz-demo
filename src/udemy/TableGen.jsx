import React, { useState } from 'react';
import {max} from 'd3';
import { Table, Input, InputNumber, 
	Typography, Button, Popconfirm, 
	Form, Row, Col,
} from 'antd';
import { 
    EditOutlined, DeleteOutlined,
} from '@ant-design/icons';
import "../css/udemy-scatterplot/Table.css";

// const GenderSelect = () => {
// 	return (
// 		<div>OK</div>
// 	)
// };

const EditableCell = (props) => {
	const {
		editing,
		dataIndex,
		title,
		inputType,
		record,
		index,
		children,
		...restProps
	} = props;
	// const inputNode = inputType === 'number' ? <InputNumber /> : inputType === 'gender' ? <GenderSelect /> : <Input />;
	const inputNode = inputType === 'number' ? <InputNumber /> : inputType === 'gender' ? <Input /> : <Input />;
	return (
		<td {...restProps}>
			{editing ? (
				<Form.Item
					name={dataIndex}
					style={{
						margin: 0,
					}}
					rules={[
						{
							required: true,
							message: `Please input a${title === "Age" ? 'n' : ''} ${title} !`,
						},
					]}
				>
					{inputNode}
				</Form.Item>
			) : (
				children
			)}
		</td>
	);
};

const emptyState = { temp:undefined, editingKey:'' };

const TableGen = (props) => {
	const { data: _data, setData, activeName, setActiveName } = props;
	const data = _data.map((e) => ({...e, key: e.id}));
	let maxId = max(data, d => d.id) + 1;
	const [form] = Form.useForm();
	const [state, setState] = useState({temp: undefined, editingKey: ''});
	const {temp, editingKey} = state;
	const setTemp = (temp) => setState({...state, temp,});
	const setEditingKey = (editingKey) => setState({...state, editingKey});
	const isEditing = (record) => record.id === editingKey;

	const edit = (record) => {
		form.setFieldsValue({
			name: '',
			age: '',
			height: '',
			...record,
		});
		setEditingKey(record.id);
	};

	const cancel = () => {
		if (!!temp)
			setState(emptyState);
		else
			setEditingKey('');
		form.resetFields();
	};

	const save = async (key) => {
		try {
			const row = await form.validateFields();
			const newData = [...data];
			const index = newData.findIndex((item) => key === item.id);
			console.log(row, temp);
			if (index > -1) {
				const item = newData[index];
				newData.splice(index, 1, { ...item, ...row });
			} else {
				newData.push({id: temp.id, ...row});
			}
			setData(newData);
			setState(emptyState)
			form.resetFields();
		} catch (errInfo) {
			console.log('Validate Failed:', errInfo);
		}
	};

	const columns = [
		{
			title: 'Name',
			dataIndex: 'name',
			key: 'name',
			editable: true
		},
		{
			title: 'Age',
			dataIndex: 'age',
			key: 'age',
			editable: true
		},
		{
			title: 'Height',
			dataIndex: 'height',
			key: 'height',
			editable: true
		},
		{
			title: 'Gender',
			dataIndex: 'gender',
			key: 'gender',
			editable: true
		},
		{
			title: 'Actions',
			dataIndex: 'actions',
			width: 190,
			render: (_, record) => {
				const actions = [];
				const editable = isEditing(record);
				actions.push(editable ? (
					<span>
						<Button
							type="link"
							onClick={() => save(record.key)}
							style={{
								marginRight: 8,
							}}
						>
							Save
						</Button>
						<Popconfirm title="Sure to cancel?" onConfirm={cancel}>
							<Button type="link">Cancel</Button>
						</Popconfirm>
					</span>
				) : (
					<Typography.Link disabled={editingKey !== ''} onClick={() => edit(record)}>
						<EditOutlined /> Edit
					</Typography.Link>
				));
				if (data.length >= 1 && !editable) {
					actions.push(
						<Popconfirm title="Sure to delete?" onConfirm={() => handleDelete(record.id)}>
							<Typography.Link 
								disabled={editingKey !== ''} 
								style={{
									paddingLeft: 10
								}}
								type="danger"
							>
								<DeleteOutlined /> Delete
							</Typography.Link>
						</Popconfirm>
					);
				}
				return (
					<div>
						{actions}
					</div>
				);
			}
		},
	];

	const handleDelete = (key) => {
		if (!!temp && key === temp.id) {
			setTemp();
			setEditingKey('');
		} else {
			const filterF = item => {
				return item.id!== key;
			}
			const newData = data.filter(filterF);
			setData(newData);
		}
	};

	const handleAdd = () => {
		setState({
			temp: { name: '', age: '', height: '', gender: '', id: maxId },
			editingKey: maxId,
		});
		maxId = maxId + 1;
	};

	const handleSave = (row) => {
		if (!temp) {
			const newData = [...data];
			const index = newData.findIndex((item) => row.key === item.key);
			const item = newData[index];
			newData.splice(index, 1, { ...item, ...row });
			setData(newData);
		} else {
			setData([...data, {...row}]);
			setTemp();
		}
	};

	const components = {
		body: {
			cell: EditableCell,
		},
	};

	const cols = columns.map((col) => {
		if (!col.editable) {
			return col;
		}

		return {
			...col,
			onCell: (record) => ({
				record,
				dataIndex: col.dataIndex,
				inputType: col.dataIndex === 'name' ? 'text' : col.dataIndex === 'gender' ? 'gender' : 'number',
				title: col.title,
				editing: isEditing(record),
				handleSave: handleSave,
			}),
		};
	});
	return (
		<Form form={form} component={false}>
			<Row justify="end">
				<Button
					onClick={handleAdd}
					type="primary"
					style={{ marginBottom: 16, }}
					disabled={editingKey !== ''}
				>
					Add a child
				</Button>
			</Row>
			<Row>
				<Col span={24}>
					<Table
						components={components}
						rowClassName={({id}) => id === activeName ? "active-row" : ''}
						bordered
						size="small"
						dataSource={!!temp ? [...data, temp]: data}
						columns={cols}
						pagination={{
							onChange: cancel,
						}}
						onRow={(record, rowIndex) => {
							return {
								onMouseEnter: event => {
									setActiveName(record.id)
								},
								onMouseLeave: event => {
									setActiveName(null)
								}
							}
						}}
					/>
				</Col>
			</Row>
		</Form>
	);
}

export default TableGen;